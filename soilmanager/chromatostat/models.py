from django.db import models
from sorl.thumbnail import ImageField
from django.contrib.auth.models import User
from sorl.thumbnail import get_thumbnail
from django.utils.safestring import mark_safe


class Chromascan(models.Model):

    short_description = models.CharField(max_length=70)
    full_description = models.TextField(blank=True)

    date = models.DateTimeField(auto_now_add=True)

    scan = ImageField(upload_to='scans/%Y/%m/%d/')

    threshold = models.FloatField(null=True, blank=True)

    fractal_dimention = models.FloatField(null=True, blank=True)

    lat = models.FloatField(null=True, blank=True)
    lon = models.FloatField(null=True, blank=True)

    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def thumbnail(self):
        return mark_safe("<img src='%s' />"
                         % get_thumbnail(self.scan, '100x100').url)
