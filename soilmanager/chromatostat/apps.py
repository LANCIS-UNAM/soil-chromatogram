from django.apps import AppConfig


class ChromatostatConfig(AppConfig):
    name = 'chromatostat'
