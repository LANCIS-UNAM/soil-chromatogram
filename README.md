# Chromatostat

## A soil chromatogram analysis tool

Use color-space analysis and fractal dimension of a chromatogram as proxies
of soil composition.

Part of the soilmanager suite.

## User story

This program might be an interactive web application, available for public use.

0. user points browser to http://soil.lancis.ecologia.unam.mx/chromatostat
1. uploads scanned chromatogram
2. adjusts color balance thresholds
    1. Color bance adjusted image is shown, which will be fed to colorspace analysis
    2. Grayscale image is shown, which will be fed to box-count
3. when levels ready, submits adjusted images for analyses
4. downloads PDF report


<https://www.etymonline.com/word/-stat?ref=etymonline_crossreference>


## References


*  Here's an example of [RGB Image Analysis with Python](http://marksolters.com/programming/2015/02/27/rgb-histograph.html).

*  Here's an example of [box count in python](https://stackoverflow.com/questions/44793221/python-fractal-box-count-fractal-dimension) for computing the [fractal dimension](https://en.wikipedia.org/wiki/Minkowski%E2%80%93Bouligand_dimension) of an image.

*  Related paper: [Case Based Interpretation of Soil Chromatograms](https://link.springer.com/chapter/10.1007/978-3-540-85502-6_40)

